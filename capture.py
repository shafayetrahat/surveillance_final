import cv2
class Capture:
    def __init__(self):
        self.capturing = False
        self.c = cv2.VideoCapture(0)

    def startcapture(self):
        print("pressed start")
        self.capturing = True
        cap = self.c
        while self.capturing:
            ret, frame = cap.read()
            cv2.imshow("Capture", frame)
            cv2.waitKey(5)
        cv2.destroyAllWindows()

    def endCapture(self):
        print("pressed End")
        self.capturing = False
        # cv2.destroyAllWindows()

    def quitcapture(self):
        print("pressed Quit")
        cap = self.c
        cv2.destroyAllWindows()
        cap.release()
        # QtCore.QCoreApplication.quit()
