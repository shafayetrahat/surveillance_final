# -*- coding: utf-8 -*
import numpy as np
import os
import tensorflow as tf
import cv2
import sys
from capture import *
from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as vis_util




CWD_PATH = os.getcwd()
sys.path.append(CWD_PATH)
SLIM_PATH = os.path.join(CWD_PATH,'slim')
sys.path.append(SLIM_PATH)


MODEL_NAME = 'pedestrian_detection'
PATH_TO_CKPT = MODEL_NAME + '/frozen_inference_graph.pb'
PATH_TO_LABELS = os.path.join(CWD_PATH,'label_map.pbtxt')

NUM_CLASSES = 1 


label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)


def detect_objects(image_np, sess, detection_graph):
    image_np_expanded = np.expand_dims(image_np, axis=0)
    image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

    boxes = detection_graph.get_tensor_by_name('detection_boxes:0')


    scores = detection_graph.get_tensor_by_name('detection_scores:0')
    classes = detection_graph.get_tensor_by_name('detection_classes:0')
    num_detections = detection_graph.get_tensor_by_name('num_detections:0')


    (boxes, scores, classes, num_detections) = sess.run(
        [boxes, scores, classes, num_detections],
        feed_dict={image_tensor: image_np_expanded})


    vis_util.visualize_boxes_and_labels_on_image_array(
        image_np,
        np.squeeze(boxes),
        np.squeeze(classes).astype(np.int32),
        np.squeeze(scores),
        category_index,
        use_normalized_coordinates=True,
        line_thickness=1)
    print(max(np.squeeze(scores)))
    return image_np

if __name__ == '__main__':
    ca = Capture()
    cap = ca.c

    is_output = False
    
    detection_graph = tf.Graph()
    with detection_graph.as_default():
        od_graph_def = tf.compat.v1.GraphDef()
        with tf.io.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
            serialized_graph = fid.read()
            od_graph_def.ParseFromString(serialized_graph)
            tf.import_graph_def(od_graph_def, name='')
            
            
    with detection_graph.as_default():
        with tf.compat.v1.Session(graph=detection_graph) as sess:   
            ret=True
            while (ret):
                ret, image_np=cap.read()
                image_np = detect_objects(image_np,sess,detection_graph)
                
                cv2.imshow('Surveillance',image_np)
                if cv2.waitKey(25) & 0xFF==ord('q'):
                    ca.endCapture()
                    ca.quitcapture()
                    break
    cv2.destroyAllWindows()
    cap.release()
