from __future__ import unicode_literals
import os
import cv2
import numpy as np
from PyQt5 import QtCore, QtGui, QtWidgets, QtSerialPort
from PyQt5.QtWidgets import (QApplication, QMainWindow, QPushButton, QWidget,
                             QLabel, QVBoxLayout)              # +++

from Surveillance import *                                  # +++


import tensorflow as tf
import sys


from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as vis_util


import sys
import os
import random
import matplotlib
from numpy import arange, sin, pi
from PyQt5.QtWidgets import*
from PyQt5.uic import loadUi

from matplotlib.backends.backend_qt5agg import (NavigationToolbar2QT as NavigationToolbar)

import numpy as np
import random
from threading import Thread
import serial
from capture import *
import pyautogui

class Video (QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowState(QtCore.Qt.WindowMaximized)
        self.setupUi(self)
        self.ser = serial.Serial('/dev/ttyUSB0', baudrate=9600)
        self.MplWidget.hide()
        self.timer2 = QtCore.QTimer(self)
        self.timer2.setInterval(5)
        self.timer2.timeout.connect(self.update_graph)
        self.start_radar()
        # self.pushButton_4.clicked.connect(self.start_radar)
        # self.pushButton_5.clicked.connect(self.poweroff)

    def start_radar(self):
        # if self.capture.capturing == True:
        pyautogui.click(x=1000, y=550)
        pyautogui.keyDown('q')
        pyautogui.keyUp('q')
        self.MplWidget.showMaximized()
        self.timer2.start()

    def update_graph(self):
        try:
            ser_bytes = self.ser.readline().decode('ascii').strip('\n').strip('\r')
        except UnicodeDecodeError:
            ser_bytes = self.ser.readline().decode('utf-8').strip('\n').strip('\r')
        data = ser_bytes.split(",")
        data = list(map(int, data))
        self.MplWidget.canvas.axes.clear()
        self.MplWidget.canvas.axes.set_ylim([0, 400])
        self.MplWidget.canvas.axes.set_yticks(np.arange(0, 420, 70))
        self.MplWidget.canvas.axes.set_thetamin(0)
        self.MplWidget.canvas.axes.set_thetamax(180)
        self.MplWidget.canvas.axes.scatter(np.deg2rad(data[0]), data[1], c='#165775', s=15, cmap='hsv', alpha=0.75)
        self.MplWidget.canvas.draw()
        print(data)

if __name__ == '__main__':

    app = QtWidgets.QApplication(sys.argv)
    window = Video()
    window.setWindowTitle('main code')
    window.showMaximized()
    sys.exit(app.exec_())
